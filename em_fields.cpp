#include <cmath>

#include "em_basics.hpp"

std::atomic<int> Window::_counter{0};


em_fields::em_fields (
  std::shared_ptr<Window> &w) :
  fBase(product<dimarray::value_type, 3>(w->dimensions)),
  window(w)
{
	window->clients.push_back(this);
}
		

em_fields::em_fields (
  em_fields const &e) :
  fBase(e.size()),
  window(e.window)
{
	window->clients.push_back(this);
}

/*
em_fields::em_fields (
  em_fields const& e,
  fields&& f)
  :	em_fields(e)
{
	memcpy(comp, f.comp, 3 * sizeof(uintptr_t));
}
*/
Window::Window (
  dimarray const& d,
  double frequency,
  std::shared_ptr<zdouble> g_kernel, 
  std::shared_ptr<fft_resources> fftr,
  vec3<double> &position,
  vec3<double> &axis) :
  _window_id(_counter.fetch_add(1, std::memory_order_relaxed)),
  frequency(frequency),
  dimensions(d),
  position(position),
  axis(axis),
  g_kernel(g_kernel),
  fftr(fftr)
{
}


/*
em_field_add::operator em_fields() const
{
	return result(src1, src1 + src2);
}


em_field_add operator +(
  em_fields const& __restrict src1,
  em_fields const& __restrict src2)
{
	assert(src1.frequency == src2.frequency);
	assert(src1.stepsize == src2.stepsize);
	assert(src1.size == src2.size);

	return em_field_add(src1, src2);
}
*/
