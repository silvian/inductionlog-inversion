#include "em_basics.hpp"
#include <lauxlib.h>
#include <lualib.h>
#include <atomic>
#include <mutex>

std::mutex w_lock;

std::atomic_flag write_lock = ATOMIC_FLAG_INIT;
std::atomic_flag read_lock = ATOMIC_FLAG_INIT;

Contrast::Contrast (
  std::shared_ptr<ConductivityModel>& m) :
	_readers(0),
	_model(m.get()),
	_val(nullptr)
{
}

void Contrast::rasterize_model(Window const* w)
{
	_model->setGridCoordinates(w);
	_val = _model->rasterize();
}


double Contrast::operator[] (
  std::size_t i) const
{
	/* As long as there is a ContrastAccess object in the same (super)scope
	 * as where we use this function, _readers != 0. Preferably, this should
	 * be checked at compile-time. But how? */
	if (_readers != 0)
		return _val[i];
	else
		throw;
}


ContrastAccess::ContrastAccess (Contrast& x, em_fields const& fem) :
	_X(x)
{
	std::lock_guard<std::mutex> lock(w_lock);
//	std::atomic_fetch_add(&_x._readers, 1);
	std::atomic_fetch_add(&_X._readers, 1);

	if (_ext_id != fem.window->get_id() || _X._model->isModified()) {
		/*Block any rasterization attemps as there are _readers active*/
		while (_X._readers.load() != 1)
			;
		_X.rasterize_model(fem.window.get());
		_ext_id = fem.window->get_id();
	}
}

ContrastAccess::~ContrastAccess()
{
	std::atomic_fetch_sub(&_X._readers, 1);
}


ConductivityModel::ConductivityModel (
  std::string const& lua_code, 
  std::vector<double>&& init,
  std::size_t N) :
	L(luaL_newstate()),
	parameters(registerNumArray(L, N, "parameters")),
	modified(true)
{
        /* opens the basic libraries */
        luaL_openlibs(L);
        luaUopen_array(L);

	if (0 != luaL_dostring(L, lua_code.c_str()))
		throw;

	parameters = registerNumArray(L, N, "parameters");
}

std::string ConductivityModel::getModelByteCode()
{
	struct bytebuffer
	{
        	size_t buf_len;
		int sp;
        	char *buffer;
		bytebuffer() : 
			buf_len(0), sp(0), buffer(nullptr) {}
	};

	struct bytebuffer bytecode_buffer;
	lua_getglobal(L, "contrast");
	lua_dump(L, [] (lua_State *L, const void *p, size_t sz, void *ud)
	{
	        struct bytebuffer *bb = (struct bytebuffer *) ud;

		if (bb->buf_len == 0) {
			bb->buf_len = 1024;
		        bb->buffer = reinterpret_cast<char *>
			  (malloc(bb->buf_len * sizeof(char)));
		} else if (bb->sp + sz > bb->buf_len) {
			bb->buf_len += 1024;
			bb->buffer = reinterpret_cast<char *> 
			  (realloc(bb->buffer, bb->buf_len *
			  sizeof(char)));
		}

	        if (bb->buffer == NULL)
        	        return 1;

	        memcpy(&bb->buffer[bb->sp], p, sz * sizeof(char));
        	bb->sp += sz;

	        return 0;
	}, reinterpret_cast<void *>(&bytecode_buffer));
	lua_pop(L, 2);

	std::string out(std::move(bytecode_buffer.buffer),
	  bytecode_buffer.buf_len);

	/* Check if move was done correctly */
	assert(bytecode_buffer.buffer == nullptr);
	return out;
}


void ConductivityModel::set (const std::vector<double>& p)
{
	assert(p.size() == parameters->size);
	for (size_t i = 0; i < p.size(); i++)
		parameters->values[i] = p[i];
	modified = true;
}


double *ConductivityModel::rasterize()
{
        lua_pop(L, 1);
       	/* Push function to the stack and execute it */
        lua_getglobal(L, "contrast");
       	if (0 != lua_pcall(L, 0, 1, 0))
		throw(lua_tostring(L, -1));

	modified = false;

       	return reinterpret_cast<struct NumArray *>(lua_touserdata(L, -1))->values;
}

void ConductivityModel::setGridCoordinates(Window const* w)
{
	// GC will remove the old x, y, and z objects
	std::size_t n = product(w->dimensions);
	std::array<struct NumArray *, 3> coor = {
		registerNumArray(L, n, "x"),
		registerNumArray(L, n, "y"),
		registerNumArray(L, n, "z")
	};
	
	std::array<vec3<double>, 3> bases = {
		base_0(w->axis), base_1(w->axis), base_2(w->axis)};

	for (size_t i = 0; i < 3; i++)
		rotated_grid(coor[i]->values, bases, w, i);
}
