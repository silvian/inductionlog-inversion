#pragma once

#include <complex>
#include <cstddef>
#include <cassert>
#include <cstring>
#include <iostream>
#include <array>
#define ALIGNMENT 32
#define VLEN 4 * sizeof(double)

typedef double __attribute__ ((vector_size(VLEN))) vec2z;

#define COMP_UNROLL(X) \
{\
	(vec2z *__restrict) __builtin_assume_aligned(X.comp[0], ALIGNMENT),\
	(vec2z *__restrict) __builtin_assume_aligned(X.comp[1], ALIGNMENT),\
	(vec2z *__restrict) __builtin_assume_aligned(X.comp[2], ALIGNMENT)\
}

#if defined(__AVX__)
#define CMUL cmul_avx
#define CMUL_CONJ cmul_conj_avx
#define CNORM cnorm_avx
#define CODE_INLINE static
#else
#define CMUL cmul
#define CODE_INLINE
#endif

CODE_INLINE vec2z CMUL(vec2z const& in1, vec2z const& in2);
CODE_INLINE vec2z CMUL_NORM(vec2z const& in1, vec2z const& in2);
CODE_INLINE vec2z CMUL_CONJ(vec2z const& in1, vec2z const& in2);

/* ASM multiplier routine for double _Complex type vector using the AVX
 * instruction set. GCC currently does not support double _Complex as vector
 * arguments. Thus we need to implement the complex multiplication ourselves as
 * it is differen347Gt from elementwise double-type multiplication. This is probably
 * as fast as it gets on CPUs supporting AVX. */
static inline vec2z cmul_avx (
  vec2z const& in1,
  vec2z const& in2)
{
	vec2z result;
	volatile vec2z t1, t2, t3;

	__asm__("\t"
	"vmovddup %2, %3\n\t"		/* r%3 = [c1 | c1 | c2 | c2] */
	"vshufpd $0xf, %2, %2, %4\n\t"	/* r%4 = [d1 | d1 | d2 | d2], imm = [1 1 1 1] */
	"vshufpd $0x5, %1, %1, %5\n\t"	/* r%5 = [b1 | a1 | b2 | a2] //imm = [0 1 0 1] */
	"vmulpd %1, %3, %3\n\t"		/* r%3 = [a1 * c1 |  b1 * c1 | a2 * c2 | b2 * c2] = r1 * r4 */
	"vmulpd %4, %5, %5\n\t"		/* r%5 = [b1 * d1 | a1 * d1 | b2 * d2 | a2 * d2] = r6 * r7 */
	"vaddsubpd %5, %3, %0"		/* r%0 = [ a1 * c1 - b1 * d1 | b1 * c1 + a1 * d1 |
					   a2 * c2 - b2 * d2 * | b2 * c2 + a2 * d2 ] = r5 +/- r8 */
	: "=x" (result) : "x" (in1), "x" (in2), "x" (t1), "x" (t2), "x" (t3));
	return result;
}

static inline vec2z cnorm_avx (
  vec2z const& in1)
{
	vec2z result;
	volatile vec2z t1, t2;
	vec2z const zero = {0., 0., 0., 0.};

	__asm__("\t"
	"vmulpd %1, %1, %1\n\t"		/* r%1 = [a^2 b^2 c^2 d^2] */
	"vshufpd $0x0, %2, %1, %3\n\t"	/* ry4 = [a^2 0 c^2 0] */
	"vshufpd $0xf, %2, %1, %4\n\t"	/* ry5 = [b^2 0 d^2 0] */
	"vaddpd %3, %4, %0"		/* r%0 = [a^2 + b^2, 0, c^2 + d^2, 0] */
	: "=x" (result) : "x" (in1), "x" (zero), "x" (t1), "x" (t2));

	return result;
}

static inline vec2z cmul_conj_avx (
  vec2z const& in1,
  vec2z const& in2)
{
	vec2z result;
	volatile vec2z t1, t2, t3;

	/* The uncommented lines are similar to the lines in cmul_conj*/
	__asm__("\t"
	"vmovddup %2, %3\n\t"
	"vshufpd $0xf, %2, %2, %4\n\t"
	"vshufpd $0x5, %1, %1, %5\n\t"
	"vmulpd %1, %3, %3\n\t"
	"vmulpd %4, %5, %5\n\t"		/* r9 = [ a1 * c1 + b1 * d1, b1 * c1 - a1 * d1,
					   a2 * c2 + b2 * d2,  b2 * c2 - a2 * d2 ] */
	"vaddsubpd  %3, %5, %0"
	: "=x" (result) : "x" (in1), "x" (in2), "x" (t1), "x" (t2), "x" (t3));

	return result;
}

typedef std::complex<double> zdouble;

/* Base type definitions .................................................... */
template <typename E>
class fExpr
{
public:
	vec2z _v_elem(std::size_t i, std::size_t j) const
	{
		return static_cast<E const&>(*this)._v_elem(i, j);
	}

	std::size_t size() const {
		return static_cast<E const&>(*this).size();
	}

	operator E&() {
		return static_cast<E&>(*this);
	}

	operator E const&() const
	{
		return static_cast<E const&>(*this);
	}
};

template <typename T>
class fBase
{
protected:
	std::size_t const size_;
	fBase(std::size_t size_) : size_(size_) {
		if (size_ % 2 == 1)
			throw;

		for (std::size_t i = 0; i < 3; i++)
			comp[i] = (zdouble *) aligned_alloc(ALIGNMENT, size_ *
		  sizeof(T));
	}
public:
        /* array order: [x][y][z], i.e. x changes slowest, z fastest */
        T *__restrict comp[3];
	std::size_t size() const
	{
		return size_;
	}

	~fBase()
	{
		for (std::size_t i = 0; i < 3; i++)
			free(comp[i]);
	}
};

template<typename E> double norm_2(fExpr<E> const &);

class fields : public fBase<zdouble>, public fExpr<fields>
{
public:
	fields(fields const&);
	fields(std::size_t len);
	fields();
 	int validity();
	void copy(fields &dest);
	void zero();
//	double norm() const {return norm_2(*this);}
	std::size_t size() const
	{
		return fBase::size();
	}

	vec2z _v_elem(std::size_t i, std::size_t j) const
	{
		vec2z *v = reinterpret_cast<vec2z*>(comp[i]);
		return v[j];
	}

	vec2z &_v_elem(std::size_t i, std::size_t j)
	{
		vec2z *v = reinterpret_cast<vec2z*>(comp[i]);
		return v[j];
	}

	zdouble operator()(std::size_t i, std::size_t j) const
	{
		return comp[i][j];
	}

	zdouble &operator()(std::size_t i, std::size_t j)
	{
		return comp[i][j];
	}

	template<typename E>
	fields(fExpr<E> const& fe) : fields(fe.size()) {
		vec2z *__restrict target[3] = COMP_UNROLL((*this));
		for (std::size_t i = 0; i < 3; i++)
		for (std::size_t j = 0; j < size_/2; j++)
			target[i][j] = fe._v_elem(i, j);
	}
};

/* Subtraction .............................................................. */
template <typename E1, typename E2>
class fSubt : public fExpr<fSubt<E1, E2>>
{
	E1 const& _u;
	E2 const& _v;
public:
	fSubt(fExpr<E1> const & u, fExpr<E2> const& v) : _u(u), _v(v)
	{
	}

	template <typename T>
	vec2z _v_elem(std::size_t i, std::size_t j) const
	{
		return _u(i, j) - _v(i, j);
	}

	std::size_t size() const {return _u.size();}
};

template <typename E1, typename E2>
fSubt<E1, E2> const operator-(fExpr<E1> const& u, fExpr<E2> const &v)
{
	return fSubt<E1, E2>(u, v);
}

/* Multiplication ........................................................... */
template<typename T> struct PODmult;

template <> struct PODmult<vec2z>
{
	static vec2z multiply(vec2z const& v1, vec2z const& v2)
	{
		return CMUL(v1, v2);
	}
};

template <> struct PODmult<double const&>
{
	static vec2z multiply(vec2z const& v1, double const& v2)
	{
		return v1 * v2;
	}
};

/* The fMultTraits<T> class implements how multiplication with a (em)field and
 * object T should be performed. It should contain the following items:
 *
 * typedef <typename> typeId:	The type we implement the multiplication for
 * typedef <typename> rhs:	The POD value of the r.h.s. we perform the
 * 	multiplication with.
 * typeId convert (<typename>):	If the argument to the multiplication needs to
 * be modified by the constructor of the multiplication object, that is done
 * through the convert function. Typically, no conversion is required and we can
 * just return the argument
 * rhs index(typeId, size_t, size_t) : get the element at _v at position (i, j)
 * if there such indexing, e.g. if typeId is a field. If e.g. typeId is a double
 * the return value should not depend on (i,j) at all
 */

template <typename T> struct fMultTraits;

template <> struct fMultTraits<zdouble const&>
{
	typedef vec2z const typeId;
	typedef vec2z rhs;

	static typeId convert (zdouble const& x)
	{
		return ((vec2z) {x.real(), x.imag(), x.real(), x.imag()});
	}

	static rhs _v_index(typeId _v, std::size_t i, std::size_t j)
	{
		return _v;
	}
};

template <> struct fMultTraits<double const&>
{
	typedef double const& typeId;
	typedef double const& rhs;

	static typeId convert (typeId arg)
	{
		return arg;
	}

	static rhs _v_index(typeId _v, std::size_t i, std::size_t j)
	{
		return _v;
	}
};

template <typename E> struct fMultTraits<fExpr<E>>
{
	typedef E const& typeId;
	typedef vec2z rhs;

	static typeId const& convert (typeId const& x)
	{
		return x;
	}

	static rhs _v_index(typeId _v, std::size_t i, std::size_t j)
	{
		return _v._v_elem(i, j);
	}

	static rhs index(typeId _v, std::size_t i, std::size_t j)
	{
		return _v(i, j);
	}
};

template <typename E1, typename E2>
class fMult : public fExpr<fMult<E1, E2>>
{
	typedef fMultTraits<E2> Traits;
	typedef PODmult<typename Traits::rhs> mult;

	E1 const& _u;
	typename Traits::typeId _v;
public:
	fMult(fExpr<E1> const & u, E2 const& v) :
	  _u(u),
	  _v(Traits::convert(v))
	{
	}

	vec2z _v_elem(std::size_t i, std::size_t j) const
	{
		return mult::multiply(_u._v_elem(i, j),
		  Traits::_v_index(_v, i, j));
	}

	zdouble operator()(std::size_t i, std::size_t j) const
	{
		return _u(i, j) * Traits::index(_v, i, j);
	}

	std::size_t size() const {
		return _u.size();
	}
};

template <typename E1, typename E2>
fMult<E1, E2> const operator* (fExpr<E1> const& u, fExpr<E2> const &v)
{
	return fMult<E1, E2>(u, v);
}

template <typename E1, typename T>
fMult<E1, T const&> const operator* (fExpr<E1> const& u, T const& v)
{
	return fMult<E1, T const&>(u, v);
}

template <typename E1, typename T>
fMult<E1, T const&> const operator* (T const& v, fExpr<E1> const& u)
{
	return fMult<E1, T const&>(u, v);
}

/* Conjugate fExpr -----------------------------------------------------------*/
template<typename E>
class fConj : public fExpr<fConj<E>>
{
	E const& _u;
public:
	fConj(fExpr<E> const& u) : _u(u) {}

	vec2z _v_elem(std::size_t i, std::size_t j) const
	{
		vec2z const& _in = _u(i, j);
		vec2z const out = {_in[0], -_in[1], _in[2], -_in[3]};
		return out;
	}

	std::size_t size() const {return _u.size();}
};

template <typename E>
fConj<E> conj(fExpr<E> const& u) {return fConj<E>(u);}

/*Add ....................................................................... */
template <typename E1, typename E2>
class fSum : public fExpr<fSum<E1, E2>>
{
	E1 const& _u;
	E2 const& _v;
public:
	fSum(fExpr<E1> const & u, fExpr<E2> const& v) : _u(u), _v(v)
	{
		assert(u.size() == v.size());
	}

	vec2z _v_elem(std::size_t i, std::size_t j) const
	{
		return _u._v_elem(i, j) + _v._v_elem(i, j);
	}

	std::size_t size() const {return _u.size();}
};

/* Cross product ............................................................ */
template <typename E1, typename E2>
class fCross : public fExpr<fCross<E1, E2>>
{
	E1 const& _u;
	E2 const& _v;
public:
	fCross(fExpr<E1> const & u, fExpr<E2> const& v) : _u(u), _v(v)
	{
		assert(u.size() == v.size());
	}

	vec2z _v_elem(std::size_t i, std::size_t j) const
	{
		return CMUL(_u._v_elem((i+1)%3, j), _v._v_elem((i+2)%2, j)) -
		  CMUL(_u._v_elem((i + 2) % 3, j), _v._v_elem((i + 1) % 2, j));
	}

	std::size_t size() const {return _u.size();}
};

template <typename E1, typename E2>
fCross<E1, E2> cross(fExpr<E1> const& u, fExpr<E2> const& v)
{
	return fCross<E1, E2>(u, v);
}

/* Miscellaneous ............................................................ */
template<typename E>
double norm_2(fExpr<E> const & fe)
{
	std::size_t len = fe.size() / 2;

	vec2z acc = {0., 0., 0., 0.};

	#pragma omp parallel default(shared)
	{
		#pragma omp for collapse(2) schedule(static, CHUNK_SIZE)
		for (std::size_t i = 0; i < 3; i++)
		for (std::size_t j = 0; j < len; j++) {
			vec2z val = fe(i, j);
			acc += CNORM(val);
		}
	}

	return acc[0] + acc[2];
}

template<typename E1, typename E2>
zdouble inner(fExpr<E1> const& f1, fExpr<E2> const& f2)
{
	std::size_t len = f1.size() / 2;
	vec2z acc = {0., 0., 0., 0.};

	#pragma omp parallel default(shared)
	{
		#pragma omp for collapse(2) schedule(static, CHUNK_SIZE)
		for (std::size_t i = 0; i < 3; i++)
		for (std::size_t j = 0; j < len; j++)
			acc += CMUL_CONJ(f1._v_elem(i, j), f2._v_elem(i, j));
	}

	return zdouble(acc[0] + acc[2], acc[1] + acc[3]);
}
