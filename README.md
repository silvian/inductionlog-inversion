# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository should contain the code for the inversion log project using CES.
It currently doesn't have any interface yet, so test interfaces have to be written on the fly.


### How do I get set up? ###

Make scripts should be provided through CMake. All dependencies must preferable be provided through CMake packages.
Project depends on c++11 so stick with a modern compiler.
Current dependencies:
libhdf5
liblua5.1
libfftw3

Testing procedures come in later hopefully :-)

### Contribution guidelines ###
Stick to these coding guidelines: https://www.kernel.org/doc/Documentation/CodingStyle
