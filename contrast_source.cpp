#include <memory>
#include <cstring>
#include <cstdlib>

#include "em_basics.hpp"

zdouble *kernel_convolution(em_fields &field, std::size_t i, zdouble *out);

static void circular_extent (zdouble const *in, zdouble *out, dimarray const& dim);
static void extract_subset(zdouble const *in, zdouble *out, dimarray const& dim);
em_fields gradient_divergence(em_fields const&);



ContrastSource::ContrastSource(std::vector<zdouble>&& mdata) : 
	_mdata(std::move(mdata))
{
	fields f1(10);
	fields f3 = f1 * 2.;
	std::cout << f3.comp[0][0] << std::endl;
}



ElectricField ContrastSource::toScatteredField()
{
	em_fields vector_potential(*this);

	for (std::size_t i = 0; i < 3; i++)
		 kernel_convolution(*this, i, vector_potential.comp[i]);

	em_fields grad_div_vpot = gradient_divergence(vector_potential);
	return ElectricField(std::move(window->k0_2 * vector_potential + grad_div_vpot));
}

zdouble *kernel_convolution (
  em_fields &fld,
  std::size_t i,
  zdouble *out)
{
	void *buffer = aligned_alloc(ALIGNMENT,
	  sizeof(zdouble) * 8 * fld.size());
	zdouble *extended_field = (zdouble *) 
	  assume_align_(new(buffer) zdouble);

	fftw_complex *temp = (fftw_complex *) fftw_malloc(8 * fld.size());

	circular_extent(fld.comp[i], extended_field, fld.window->dimensions);

	fftw_execute_dft(fld.window->fftr->forward_inplace,
	  (fftw_complex *) extended_field, (fftw_complex *) extended_field);
	fftw_execute_dft(fld.window->fftr->forward,
	  (fftw_complex *) fld.window->g_kernel.get(), temp);

	for (std::size_t n = 0; n < 4 * fld.size(); n++)
		extended_field[n] *= ((zdouble *) temp)[n];

	fftw_execute_dft(fld.window->fftr->backward_inplace,
	  (fftw_complex *) extended_field, (fftw_complex *) extended_field);
	extract_subset(extended_field, out, fld.window->dimensions);

	extended_field->~zdouble();
	free(buffer);
	fftw_free(temp);

	return out;
}

void circular_extent (
  zdouble const *in,
  zdouble *out,
  dimarray const& dim)
{	
	memset(out, 0, 8 * dim[0] * dim[1] * dim[2] * sizeof(zdouble));

	zdouble const *_in = (zdouble *) assume_align_(in);
	zdouble *_out = (zdouble *) assume_align_(out);

	for (std::size_t i = 0; i < dim[0]; i++)
	for (std::size_t j = 0; j < dim[1]; j++) {
		std::size_t fullset_index = dim[2] * (j + dim[1] * i);
		std::size_t superset_index = 2 * dim[2] * (j + 2* dim[1] * i); 
		std::memcpy(_out + superset_index, _in + fullset_index, 
		  dim[2] * sizeof(zdouble));
	}
}

void extract_subset (
  zdouble const *in,
  zdouble *out,
  dimarray const& dim)
{	
	zdouble const *_in = (zdouble *) assume_align_(in);
	zdouble *_out = (zdouble *) assume_align_(out);

	for (std::size_t i = 0; i < dim[0]; i++)
	for (std::size_t j = 0; j < dim[1]; j++) {
		std::size_t subset_index = dim[2] * (j + dim[1] * i);
		std::size_t fullset_index = 2 * dim[2] * (j + 2* dim[1] * i); 

		std::memcpy(_out + subset_index, _in + fullset_index, 
		  dim[2] * sizeof(zdouble));
	}
}
