#pragma once

#include <memory>
#include <cstddef>
#include <complex>
#include <fftw3.h>
#include <array>
#include <vector>
#include <string>
#include <lua.h>
#include <atomic>
#include "fields.hpp"
#include <boost/utility.hpp>
#include "NumArray.h"
#include <eigen3/Eigen/Dense>
#include "vec3.hpp"

class Contrast;
class ElectricField;
class MagneticField;
class em_fields;

#define assume_align_(X) __builtin_assume_aligned((X), ALIGNMENT)

static const zdouble i_(0., 1.);

typedef std::array<size_t, 3>  dimarray;
typedef vec3<zdouble> em_value;
typedef double __attribute__ ((vector_size(VLEN/2))) vec2d;
class fft_resources
{
public:
	fft_resources();
	~fft_resources();
	void editPlan(dimarray const &);

	fftw_plan forward, backward, forward_inplace, backward_inplace;
};

class ContrastSource;

class Window
{
	int _window_id;
	static std::atomic<int> _counter;
	double background;
public:
	const vec3<double> position;
	const vec3<double> axis;
	std::vector<em_fields*> clients;
	double const frequency;
	zdouble k0;
	zdouble k0_2;
	dimarray dimensions;
	std::shared_ptr<zdouble> g_kernel;
	std::shared_ptr<fft_resources> fftr;
	double stepsize;

	Window(dimarray const& d, double frequency,
	  std::shared_ptr<zdouble> g_kernel,
	  std::shared_ptr<fft_resources> fftr, vec3<double> &position, vec3<double> &axis);
	int get_id() const {return _window_id;}
	void modify()
	{
		_window_id = _counter.fetch_add(1, std::memory_order_relaxed);
	}

	const double &getBackground() const
	{
		return background;
	}

	void setBackground(double b) {
		background = b;
 		k0_2 = i_ * 4e-7 * M_PI * 2. * M_PI * frequency * b;
		k0_2 = std::sqrt(k0);
	}

};

class ConductivityModel : boost::noncopyable
{
	friend class Contrast;

	lua_State *L;
	struct NumArray *parameters;

	bool modified;

	void setGridCoordinates(Window const*);
	double *rasterize();
public:
	ConductivityModel(std::string const& LuaBytecode,
	  std::vector<double>&& init, std::size_t N);
	std::string getModelByteCode();
	bool isModified() const {return modified;};
	void set(const std::vector<double>& p);
	std::vector<double> get() const;
};

class Contrast
{
	friend class ContrastAccess;
	void rasterize_model(Window const* w);
	std::atomic<int> _readers;
	ConductivityModel *_model;
	double *_val;
public:
	Contrast(std::shared_ptr<ConductivityModel>&);
	double operator[](std::size_t i) const;
	vec2d _v_elem(std::size_t i) const
	{
		vec2d *tmp = reinterpret_cast<vec2d *> (_val);
		return tmp[i];
	}
};

class ContrastAccess : boost::noncopyable
{
	Contrast& _X;
	int _ext_id;
public:
	ContrastAccess(Contrast&, em_fields const&);
	~ContrastAccess();
};

/* Base type definitions .................................................... */
template <typename E>
class emExpr : public fExpr<E>
{
public:
	const std::shared_ptr<Window>& getWindow() const
	{
		return static_cast<E const&>(*this).getWindow();
	}

	em_value operator()(size_t i, size_t j, size_t k) const
	{
		return static_cast<E const&>(*this)(i, j, k);
	}
};

class em_fields : public fBase<zdouble>, public emExpr<em_fields>
{
	friend zdouble *kernel_convolution(em_fields &, std::size_t, zdouble *);
protected:
	em_fields() : fBase(0) {}
public:
	std::shared_ptr<Window> window;

	em_fields(em_fields const &);
	em_fields(std::shared_ptr<Window>&);
//	explicit em_fields(em_fields const &, fields&&);
//	em_fields(em_fields &&);

	auto getWindow() const -> const decltype(window) &
	{
		return window;
	}

	std::size_t size() const
	{
		return fBase::size();
	}

	vec2z _v_elem (std::size_t i, std::size_t j) const
	{
		vec2z *v = reinterpret_cast<vec2z *>(comp[i]);
		return v[j];
	}

	vec2z _v_elem (std::size_t i, std::size_t j)
	{
		vec2z *v = reinterpret_cast<vec2z *>(comp[i]);
		return v[j];
	}

	em_value operator()(size_t i, size_t j, size_t k) const
	{
		std::size_t n = k + window->dimensions[2] * 
		  (j + window->dimensions[1] * i);
		return em_value(comp[0][n], comp[1][n], comp[2][n]);
	}

	template<typename E>
	em_fields(emExpr<E> const& fem) :
		fBase<zdouble>(fem.size()),
		window(fem.getWindow())
	{
		vec2z *__restrict target[3] = COMP_UNROLL((*this));
		for (std::size_t i = 0; i < 3; i++)
		for (std::size_t j = 0; j < size_/2; j++)
			target[i][j] = fem._v_elem(i, j);
	}

	template<typename E>
	em_fields &operator +=(emExpr<E> const& fem)
	{
		vec2z *__restrict target[3] = COMP_UNROLL((*this));
		for (std::size_t i = 0; i < 3; i++)
		for (std::size_t j = 0; j < size_/2; j++)
			target[i][j] += fem._v_elem(i, j);
		return *this;
	}

	template<typename E>
	em_fields &operator -=(emExpr<E> const& fem)
	{
		vec2z *__restrict target[3] = COMP_UNROLL((*this));
		for (std::size_t i = 0; i < 3; i++)
		for (std::size_t j = 0; j < size_/2; j++)
			target[i][j] -= fem._v_elem(i, j);
		return *this;
	}

	template<typename E>
	em_fields &operator =(emExpr<E> const& fem)
	{
		vec2z *__restrict target[3] = COMP_UNROLL((*this));
		for (std::size_t i = 0; i < 3; i++)
		for (std::size_t j = 0; j < size_/2; j++)
			target[i][j] = fem._v_elem(i, j);
		return *this;
	}
};

/* Summation ---------------------------------------------------------------- */
template <typename E1, typename E2>
class emSum : public emExpr<emSum<E1, E2>>
{
	E1 const& _u;
	fSum<E1, E2> const fs;
public:
	explicit emSum(emExpr<E1> const& u, emExpr<E2> const& v) :
		_u(u),
		fs(static_cast<fExpr<E1> const&>(u),
		  static_cast<fExpr<E2> const&>(v))
	{
		assert(u.getWindow() == v.getWindow());
	}

	emSum(emExpr<E1> const& u, fExpr<E2> const& v) :
	  _u(u),
	  fs(static_cast<fExpr<E1> const&>(u), v)
	{
	}

	auto getWindow() const -> decltype(_u.getWindow()) &
	{
		return _u.getWindow();
	}

	std::size_t size() const
	{
		return _u.size();
	}

	em_value operator()(size_t i, size_t j, size_t k) const
	{
		std::size_t n = k + getWindow()->dimensions[2] * 
		  (j + getWindow->dimensions[1] * i);
		return emvalue(fs(0, n), fs(1, n), fs(2, n));
	}

	vec2z _v_elem(std::size_t i, std::size_t j) const
	{
		return fs._v_elem(i, j);
	}
};

template <typename E1, typename E2>
emSum<E1, E2> const operator+(emExpr<E1> const& u, emExpr<E2> const &v)
{
	return emSum<E1, E2>(u, v);
}

template <typename E1, typename E2>
emSum<E1, E2> const operator+(emExpr<E1> const& u, fExpr<E2> const &v)
{
	return emSum<E1, E2>(u, v);
}

template <typename E1, typename E2>
emSum<E1, E2> const operator+(fExpr<E1> const& u, emExpr<E2> const &v)
{
	return emSum<E2, E1>(v, u);
}

/* Multiplication ........................................................... */
template <> struct fMultTraits<Contrast const&>
{
	typedef Contrast const& typeId;
	typedef vec2d const rhs;

	static typeId convert (typeId arg)
	{
		return arg;
	}

	static rhs _v_index(typeId _v, std::size_t i, std::size_t j)
	{
		return _v._v_elem(j);
	}
};

template <> struct PODmult<vec2d const>
{
	static vec2z multiply(vec2z const& v1, vec2d const& v2)
	{
		vec2z tmp = {v2[0], v2[0], v2[1], v2[1]};
		return v1 * tmp;
	}
};

template <typename E1, typename E2>
class emMult : public emExpr<emMult<E1, E2>>
{
	typedef fMultTraits<E2> Traits;
	typedef PODmult<typename Traits::rhs> mult;

	E1 const& _u;
	typename Traits::typeId  _v;

public:
	emMult(emExpr<E1> const& u, E2 const& v) :
	  _u(u),
	  _v(Traits::convert(v))
	{
	}

	vec2z _v_elem(std::size_t i, std::size_t j) const
	{
		return mult::multiply(_u._v_elem(i, j), Traits::_v_index(_v, i, j));
	}

	auto getWindow() const -> decltype(_u.getWindow()) &
	{
		return _u.getWindow();
	}

	std::size_t size() const
	{
		return _u.size();
	}
};

template <typename E1, typename T>
emMult<E1, T const&> const operator* (T const &v, emExpr<E1> const& u)
{
	return emMult<E1, T const&>(u, v);
}

template <typename E1, typename T>
emMult<E1, T const&> const operator* (emExpr<E1> const& u, T const &v)
{
	return emMult<E1, T const&>(u, v);
}

/* Conjugation .............................................................. */
template<typename E>
class emConj : public emExpr<emConj<E>>
{
	E const& _u;
public:
	emConj(emExpr<E> const& u) :
		_u(u)
	{
	}

	vec2z _v_elem(std::size_t i, std::size_t j) const
	{
		vec2z const& _in = _u(i, j);
		vec2z const out = {_in[0], -_in[1], _in[2], -_in[3]};
		return out;
	}

	auto getWindow() const -> decltype(_u.getWindow()) &
	{
		return _u.getWindow();
	}

	std::size_t size() const
	{
		return _u.size();
	}
};

template <typename E>
emConj<E> conj(emExpr<E> const& u)
{
	return emConj<E>(u);
}

/* Cross product ............................................................ */
template <typename E1, typename E2>
class emCross : public emExpr<emCross<E1, E2>>
{
	E1 const& _u;
	E2 const& _v;

public:
	emCross(emExpr<E1> const& u, emExpr<E2> const& v) :
		_u(u),
		_v(v)
	{
		assert(u.size() == v.size());
	}

	vec2z _v_elem(std::size_t i, std::size_t j) const
	{
		return CMUL(_u((i+1)%3, j), _v((i+2)%2, j)) -
		  CMUL(_u((i + 2) % 3, j), _v((i + 1) % 2, j));
	}

	auto getWindow() const -> decltype(_u.getWindow()) &
	{
		return _u.getWindow();
	}

	std::size_t size() const
	{
		return _u.size();
	}
};

template <typename E1, typename E2>
emCross<E1, E2> cross(emExpr<E1> const& u, emExpr<E2> const& v)
{
	return emCross<E1, E2>(u, v);
}

/* OTHER CLASSES -------------------------------------------------------------*/

class ContrastSource : public em_fields
{
	std::vector<zdouble> _mdata;
public:
	ContrastSource(std::vector<zdouble>&&);
	ElectricField toScatteredField();
};

void assert_similarity(em_fields const& f1, em_fields const& f2);

class ElectricField : public em_fields
{
public:
	using em_fields::em_fields;
	ContrastSource& toContrastSource(Contrast&);
//	ElectricField(em_fields &&);
};

class MagneticField : public em_fields
{
public:
	using em_fields::em_fields;
};


std::array<double, 3> normal(dimarray const& d, std::size_t i1, std::size_t i2,
  std::size_t i3);

template <int shift>
vec3<double> normal (
  dimarray const& d,
  std::size_t i1,
  std::size_t i2,
  std::size_t i3)
{
	std::array<double, 3> out;
	if (shift == i1)
		out[0] = -1;
	else if(d[0] - shift == i1)
		out[0] = 1;
	else
		;
	if (shift == i2)
		out[1] = -1;
	else if(d[1] - 1 == i2)
		out[1] = 1;
	else
		;

	if (shift == i3)
		out[2] = -1;
	else if(d[2] - shift == i3)
		out[2] = 1;
	else
		;
	return out;
}


template<typename E>
zdouble boundary_integral(emExpr<E> const& u)
{
	zdouble accumulator = 0.;
	std::array<bool, 3> valid;
	const dimarray& dd = u.getWindow()->dimensions;
	for (size_t i = 1; i < dd[0] - 1; i++) {
		valid[0] = (i==1 || i ==  dd[0] - 1);
		for (size_t j = 1; j < dd[1]-1; j++) {
			valid[1] = (j==1 || j ==  dd[1] - 1);
			for (size_t k = 1; k < dd[2]-1; k++) {
				valid[2] = (k==1 || k == dd[2]-1);
				if (valid[0] || valid[1] || valid[2]) {
					accumulator = inner_product(u(i, j, k).real(),
					  normal<1>(dd, i, j, k));
				}
			}
		}
	}

	return accumulator * std::pow(u.getWindow()->stepsize, 2);
}

template<typename T, std::size_t N>
T product(std::array<T, N> in )
{
	T acc{0};
	for (const T& e : in)
		acc += e;
	return acc;
}



vec3<double> base_0(vec3<double> const& axis);
vec3<double> base_1(vec3<double> const& axis);
vec3<double> base_2(vec3<double> const& axis);
void rotated_grid(double *dest, std::array<vec3<double>, 3>const& bases, const Window* w, std::size_t N);
