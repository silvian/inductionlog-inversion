#pragma once
#include <lua.h>

#ifdef __cplusplus
extern "C" {
#endif	
struct NumArray
{
        unsigned int size;
	// Appearantly, if values is declared as a pointer it won't work with Lua
        double values[1] __attribute__((bnd_variable_size));
};

struct NumArray *registerNumArray(lua_State *L, int nelements, const char *name);
int luaUopen_array(lua_State *L);
#ifdef __cplusplus
}
#endif
