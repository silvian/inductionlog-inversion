#include <cstdlib>
#ifdef _OPENMP
#include <omp.h>
#endif

#include "em_basics.hpp"

#define CHUNK_SIZE 32

#define OMP_GRADDIV 1

static zdouble linear_index(zdouble const *, size_t, int, int, 
  int);

static zdouble apply_4th_order_stencil(size_t x, size_t y, size_t z,
  size_t or_, size_t dim, zdouble const *fcomp, double h);

static zdouble apply_2nd_order_stencil(size_t x, size_t y, size_t z, 
  size_t or_, size_t dim, zdouble const *fcomp, double h);

static zdouble apply_p_2nd_order_stencil(size_t x, size_t y, size_t z, 
  size_t or_, size_t dim, zdouble *fcomp, double h);

static zdouble apply_p_2nd_order_stencil_2(size_t x, size_t y, size_t z, 
  size_t or_, size_t dim, zdouble *fcomp, double h);

static zdouble linear_index (
  zdouble const *fcomp,
  dimarray const& dim,
  size_t x,
  size_t y,
  size_t z)
{
	return fcomp[z + dim[2] * (y + dim[1] * x)];
}

/* Return second order central deriviative in the <dim> direction */
static zdouble apply_2nd_order_stencil (
  size_t x, 
  size_t y, 
  size_t z, 
  size_t or_,
  dimarray const& dim,
  zdouble const* fcomp,
  double h)
{
	zdouble val;
	/* (n + 1) and (n - 1) terms */ 
	switch(or_) {
	case 0:
		val = linear_index(fcomp, dim, x + 1, y, z) + 
		  linear_index(fcomp, dim, x - 1, y, z);
		break;

	case 1:
		val = linear_index(fcomp, dim, x, y + 1, z) + 
		  linear_index(fcomp, dim, x, y - 1, z);
		break;

	case 2:
		val = linear_index(fcomp, dim, x, y, z + 1) + 
		  linear_index(fcomp, dim, x, y, z - 1);
		break;
	#if defined __GNU_C__	|| defined __clang__
	default:
		__builtin_trap();
	#endif
	}	
	/* Central term */
	val -= 2. * linear_index(fcomp, dim, x, y, z);

	return val / std::pow(h, 2);
}


static zdouble apply_4th_order_stencil (
  size_t x, 
  size_t y, 
  size_t z, 
  size_t or_,
  dimarray const& dim,
  zdouble const* fcomp,
  double h)
{
	zdouble points[25];

	double const weights[25] = {
	   1., -16.,   30.,  -16,   1.,
	  -16, 256., -480.,  256, -16.,
	   30, -480.,  900., -480,  30.,
	  -16, 256., -480.,  256, -16.,
	   1., -16.,   30.,  -16,   1.};
	size_t ix = 0;

	switch(or_) {
	case 0:
		for (int i = -2; i < 3; i++)
			for (int j = -2; j < 3; j++) {
				points[ix] = linear_index(fcomp, dim, x, y + i,
				  z + j);
				ix++;
			}
		break;

	case 1:
		for (int i = -2; i < 3; i++)
			for (int j = -2; j < 3; j++) {
				points[ix] = linear_index(fcomp, dim, x + i, y,
				  z + j);
				ix++;
			}
		break;

	case 2:
		for (int i = -2; i < 3; i++)
			for (int j = -2; j < 3; j++) {
				points[ix] = linear_index(fcomp, dim, x + i,
				  y + j, z);
				ix++;
			}
		break;
	#if defined __GNU_C__	|| defined __clang__
	default:
		__builtin_trap();
	#endif
	}	

	zdouble val = 0;

	for (size_t i = 0; i < 25; i++)
		val += weights[i] * points[i];

	return val / (144. * std::pow(h, 4));   
}


static zdouble apply_p_2nd_order_stencil (
  size_t x, 
  size_t y, 
  size_t z, 
  size_t or_,
  dimarray const& dim,
  zdouble const* fcomp,
  double h)
{
	zdouble val;
	switch(or_) {
	case 0:
		val = linear_index(fcomp, dim, x, y + 1, z + 1) + 
		  linear_index(fcomp, dim, x, y - 1, z - 1) -
		  linear_index(fcomp, dim, x, y + 1, z - 1) -
		  linear_index(fcomp, dim, x, y - 1, z + 1);
		break;

	case 1:
		val = linear_index(fcomp, dim, x + 1, y, z + 1) + 
		  linear_index(fcomp, dim, x - 1, y, z - 1) -
		  linear_index(fcomp, dim, x + 1, y, z - 1) -
		  linear_index(fcomp, dim, x - 1, y, z + 1);
		break;

	case 2:
		val = linear_index(fcomp, dim, x + 1, y + 1, z) + 
		  linear_index(fcomp, dim, x - 1, y - 1, z) -
		  linear_index(fcomp, dim, x - 1, y + 1, z) -
		  linear_index(fcomp, dim, x + 1, y - 1, z);
		break;
	#if defined __GNU_C__	|| defined __clang__
	default:
		__builtin_trap();
	#endif
	}	

	return val / (4. * std::pow(h, 2));
}


static zdouble apply_p_2nd_order_stencil_2 (
  size_t x, 
  size_t y, 
  size_t z, 
  size_t or_,
  dimarray const& dim,
  zdouble const* fcomp,
  double h)
{
	zdouble val;

	switch(or_) {
	case 0:
		val = linear_index(fcomp, dim, x, y + 1, z + 1) + 
		  linear_index(fcomp, dim, x, y - 1, z - 1) +
		  2. * linear_index(fcomp, dim, x, y, z) -
		  linear_index(fcomp, dim, x, y + 1, z) -
		  linear_index(fcomp, dim, x, y - 1, z) -
		  linear_index(fcomp, dim, x, y, z - 1) -
		  linear_index(fcomp, dim, x, y, z + 1);
		break;

	case 1:
		val = linear_index(fcomp, dim, x + 1, y, z + 1) + 
		  linear_index(fcomp, dim, x - 1, y, z - 1) +
		  2. * linear_index(fcomp, dim, x, y, z) -
		  linear_index(fcomp, dim, x + 1, y, z) -
		  linear_index(fcomp, dim, x - 1, y, z) -
		  linear_index(fcomp, dim, x, y, z - 1) -
		  linear_index(fcomp, dim, x, y, z + 1);

		break;

	case 2:
		val = linear_index(fcomp, dim, x + 1, y + 1, z) + 
		  linear_index(fcomp, dim, x - 1, y - 1, z) +
		  2. * linear_index(fcomp, dim, x, y, z) -
		  linear_index(fcomp, dim, x + 1, y, z) -
		  linear_index(fcomp, dim, x - 1, y, z) -
		  linear_index(fcomp, dim, x, y + 1, z) -
		  linear_index(fcomp, dim, x, y - 1, z);

		break;
	#if defined __GNU_C__	|| defined __clang__
	default:
		__builtin_trap();
	#endif	
	}	

	return val / (2. * std::pow(h, 2));
}


/* For grid sizes up to 48x48x48 it is surely not worthwhile to use OpenMP. For 
 * larger sizes it may, though. OpenMP for this routine is enabled by 
 * defining OMP_GRADDIV */
em_fields gradient_divergence (
  em_fields const& in)
{
	em_fields out(in);
	size_t n = 0;

	#ifdef OMP_GRADDIV
	#pragma omp parallel private(n)
	{
	#pragma omp for collapse(3) schedule(static, CHUNK_SIZE)
	#endif

	for (size_t x = 1; x < in.window->dimensions[0] - 1; x++)
	for (size_t y = 2; y < in.window->dimensions[1] - 1; y++)
	for (size_t z = 1; z < in.window->dimensions[2] - 1; z++) {
		#ifdef OMP_GRADDIV
		n = z + in.window->dimensions[2] * (y + in.window->dimensions[1] * x);
		#endif

		for (size_t i = 0; i < 3; i++) {
			size_t const sh1 = (i + 1) % 3;	/*[1 2 0] */
			size_t const sh2 = (i + 2) % 3;	/*[2 0 1] */

			out.comp[i][n] = 
			  apply_2nd_order_stencil(x, y, z, i, in.window->dimensions, in.comp[i], in.window->stepsize) +
			  apply_p_2nd_order_stencil_2(x, y, z, sh1, in.window->dimensions, 
			  in.comp[sh2], in.window->stepsize) +
			  apply_p_2nd_order_stencil_2(x, y, z, sh2, in.window->dimensions, 
			  in.comp[sh1], in.window->stepsize);
		}
		#ifndef OMP_GRADDIV
		n++;
		#endif
	}
	
	#ifdef OMP_GRADDIV
	}
	#endif

	return out;
}
