#include <cstring>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#ifdef _OPENMP
#include <omp.h>
#endif

#include <iostream>
#include "fields.hpp"

#define SUCCESS 0
#define FAILURE -1

#ifdef MATLAB_MEX_FILE
#define exit_ mexErrMsgTxt('Invalid input arguments detected\n')
#else
#define exit_ std::exit(EXIT_FAILURE)
#endif

/* Reference implementation. This implementation is efficient in register usage.
 * This also reduces the oppertunity for superscalar execution.
void cmul_avx(vec2z const *__restrict in1, vec2z const *__restrict in2, vec2z *__restrict out)
{
	asm("vmovapd	%1,	 %%ymm1\n\t"	   // r1 = mem @ 1
	"vmovapd	%2,	 %%ymm2\n\t"	   // r2 = mem @ 2
	"vmovddup   %%ymm2,	 %%ymm3\n\t"	   // r4 = [c1 | c1 | c2 | c2], r2 = [c1 | d1 | c2 | d2]
	"vshufpd	$0B1111,	%%ymm2, %%ymm2, %%ymm4\n\t"	 // r6 = [d1 | d1 | d2 | d2], imm = [1 1 1 1]
	"vshufpd	$0B0101,	%%ymm1, %%ymm1, %%ymm5\n\t"	 // r7 = [b1 | a1 | b2 | a2] //imm = [0 1 0 1]
	"vmulpd	 %%ymm1,	 %%ymm3, %%ymm3\n\t"	  // r5 = [a1 * c1 |  b1 * c1 | a2 * c2 | b2 * c2] = r1 * r4
	"vmulpd	 %%ymm4,	 %%ymm5, %%ymm5\n\t"	  // r8 = [b1 * d1 | a1 * d1 | b2 * d2 | a2 * d2] = r6 * r7
	"vaddsubpd  %%ymm5,	 %%ymm3, %%ymm1\n\t"	  // r9 = [ a1 * c1 - b1 * d1 | b1 * c1 + a1 * d1 |  a2 * c2 - b2 * d2 | b2 * c2 + a2 * d2 ] = r7 * r8
	"vmovapd	%%ymm1,	 %0\n\t"	// mem @ 0 = r9
	: "=m" (*out) : "m" (*in1), "m" (*in2) : "%xmm1", "%xmm2", "%xmm3", "%xmm4", "%xmm5");
}
*/

#ifndef __AVX__
vec2z cmul (
  vec2z const in1,
  vec2z const in2)
{
	vec2z result;
	/* (Re, Im), (Re, Im) */
	result[0] = in1[0] * in2[0] - in1[1] * in1[1];
	result[0] = in1[0] * in2[1] + in1[1] * in1[0];
	result[0] = in1[2] * in2[2] - in1[3] * in1[3];
	result[0] = in1[2] * in2[3] + in1[3] * in1[2];

	return result;
}
#endif


fields::fields(const fields& f) : fBase(f.size())
{
}

fields::fields(std::size_t N) : fBase(N)
{
}


int fields::validity()
{
	if (NULL == comp[0] || NULL == comp[1] ||
	  NULL == comp[2])
		return FAILURE;
	else
		return SUCCESS;
}

void fields::copy (fields& dest)
{
	if (this == &dest)
		return;

	#pragma omp parallel default(shared)
	{
	#pragma omp for schedule(static, 3)
	for (std::size_t i = 0; i < 3; i++)
		std::memcpy(dest.comp[i], comp[i], dest.size_ * sizeof(zdouble));
	}
}

void fields::zero()
{
	for (std::size_t i = 0; i < 3; i++)
		std::memset(comp[i], 0., size_ * sizeof(zdouble));
}


/* Pointwise multiply */
//field_pmf::field_pmf (
//  fields const& src,
//  double const *m) :
//src(src), m(m)
//{
//}
//
//field_pmf::operator fields() const {
//	fields dest(src.size);
//	std::size_t const len = src.size / 2;
//
//	vec2z const *__restrict s[3] = COMP_UNROLL(src);
//	vec2z *__restrict d[3] = COMP_UNROLL(dest);
//
//	#pragma omp parallel default(shared)
//	{
//	#pragma omp for collapse(2) schedule(static, CHUNK_SIZE)
//	for (size_t i = 0; i < 3; i++)
//	for (size_t j = 0; j < len; j++) {
//		vec2z temp = {m[2*j], m[2*j], m[2*j+1], m[2*j+1]};
//		d[i][j] = temp * s[i][j];
//	}
//	}
//
//	return dest;
//}
//
//field_pmf operator *(fields const& src, double const *m)
//{
//	return field_pmf(src, m);
//}
//
//field_pmf operator *(double const *m, fields const& src)
//{
//	return field_pmf(src, m);
//}
//
//
///* Complex scalar multiplication */
//field_zpmf::field_zpmf (
//  fields const& src,
//  zdouble m) :
//src(src), m(m)
//{
//}
//
//field_zpmf::operator fields() const {
//	fields result(src.size);
//	std::size_t const len = src.size / 2;
//
//	vec2z const *__restrict s[3] = COMP_UNROLL(src);
//	vec2z const mult = {m.real(), m.imag(), m.real(), m.imag()};
//	vec2z *__restrict d[3] = COMP_UNROLL(result);
//
//	#pragma omp parallel default(shared)
//	{
//	#pragma omp for collapse(2) schedule(static, CHUNK_SIZE)
//	for (std::size_t i = 0; i < 3; i++)
//	for (std::size_t j = 0; j < len; j++) {
//		d[i][j] = CMUL(s[i][j], mult);
//	}
//	}
//
//	return result;
//}
//
//field_pmf operator *(fields const& src, zdouble m)
//{
//	return field_pmf(src, m);
//}
//
//field_pmf operator *(zdouble m, fields const& src)
//{
//	return field_pmf(src, m);
//}
//
//
///* Add fields */
//field_add::field_add(
//  fields const &__restrict src1, 
//  fields const &__restrict src2)
//  : src1(src1), src2(src2)
//{
//}
//
//field_add::operator fields() const {
//	fields result(src1.size);
//
//	std::size_t const len = result.size / 2;
//
//	vec2z const *__restrict s1[3] = COMP_UNROLL(src1);
//	vec2z const *__restrict s2[3] = COMP_UNROLL(src2);
//	vec2z *__restrict d[3] = COMP_UNROLL(result);
//
//	#pragma omp parallel default(shared)
//	{
//	#pragma omp for collapse(2) schedule(static, CHUNK_SIZE)
//	for (std::size_t i = 0; i < 3; i++)
//		for (std::size_t j = 0; j < len; j++)
//			d[i][j] = s1[i][j] + s2[i][j];
//	}
//	return result;
//}
//
//
//field_add operator +(
//  fields const& __restrict src1,
//  fields const& __restrict src2)
//{
//	return field_add(src1, src2);
//}
//
//
///* Increment fields */
//void operator += (
//  fields& __restrict src1, 
//  fields const &__restrict src2)
//{
//	std::size_t const len = src1.size / 2;
//
//	vec2z *__restrict s1[3] = COMP_UNROLL(src1);
//	vec2z const *__restrict s2[3] = COMP_UNROLL(src2);
//
//	#pragma omp parallel default(shared)
//	{
//	#pragma omp for collapse(2) schedule(static, CHUNK_SIZE)
//	for (std::size_t i = 0; i < 3; i++)
//		for (std::size_t j = 0; j < len; j++)
//			s1[i][j] += s2[i][j];
//	}
//}	
//
///* Decrement fields */
//void operator -= (
//  fields& __restrict src1, 
//  fields const &__restrict src2)
//{
//	std::size_t const len = src1.size / 2;
//
//	vec2z *__restrict s1[3] = COMP_UNROLL(src1);
//	vec2z const *__restrict s2[3] = COMP_UNROLL(src2);
//
//	#pragma omp parallel default(shared)
//	{
//	#pragma omp for collapse(2) schedule(static, CHUNK_SIZE)
//	for (std::size_t i = 0; i < 3; i++)
//		for (std::size_t j = 0; j < len; j++)
//			s1[i][j] -= s2[i][j];
//	}
//}	
//
//
///* Subtract fields */
//field_subtract::field_subtract (
//  fields const &__restrict src1, 
//  fields const &__restrict src2) 
//  : src1(src1), src2(src2)
//{
//}
//
//field_subtract::operator fields() const {
//	fields result(src1.size);
//
//	std::size_t const len = result.size / 2;
//
//	vec2z const *__restrict s1[3] = COMP_UNROLL(src1);
//	vec2z const *__restrict s2[3] = COMP_UNROLL(src2);
//	vec2z *__restrict d[3] = COMP_UNROLL(result);
//
//	#pragma omp parallel default(shared)
//	{
//	#pragma omp for collapse(2) schedule(static, CHUNK_SIZE)
//	for (std::size_t i = 0; i < 3; i++)
//		for (std::size_t j = 0; j < 2 * len; j++)
//			d[i][j] = s1[i][j] - s2[i][j];
//	}
//
//	return result;
//}
//
//field_subtract operator -(
//  fields const &__restrict src1,
//  fields const &__restrict src2)
//{
//	return field_subtract(src1, src2);
//}
//
//template<>
//fields fused_multiply_add<> (
//  zdouble scalar,
//  fields const& __restrict src1,
//  fields const& __restrict src2)
//{
//	fields dest = fields(src1.size);
//	std::size_t len = src1.size / 2;
//
//	vec2z const *s1[3] = COMP_UNROLL(src1);
//	vec2z const *s2[3] = COMP_UNROLL(src2);
//	vec2z *d[3] = COMP_UNROLL(dest);
//
//	vec2z const scalar_256 = {real(scalar), imag(scalar), real(scalar),
//	  imag(scalar)};
//
//	#pragma omp parallel default(shared)
//	{
//	#pragma omp for collapse(2) schedule(static, CHUNK_SIZE)
//	for (std::size_t i = 0; i < 3; i++)
//		for (std::size_t j = 0; j < len; j++)
//			d[i][j] = CMUL(s1[i][j], scalar_256) + s2[i][j];
//	}
//
//	return dest;
//}
//
//template<>
//fields fused_multiply_add<> (
//  double scalar,
//  fields const& __restrict src1,
//  fields const& __restrict src2)
//{
//	fields dest = fields(src1.size);
//	std::size_t len = dest.size / 2;
//
//	vec2z const *s1[3] = COMP_UNROLL(src1);
//	vec2z const *s2[3] = COMP_UNROLL(src2);
//	vec2z *d[3] = COMP_UNROLL(dest);
//
//	#pragma omp parallel default(shared)
//	{
//	#pragma omp for collapse(2) schedule(static, CHUNK_SIZE)
//	for (std::size_t i = 0; i < 3; i++)
//		for (std::size_t j = 0; j < len; j++)
//			d[i][j] = scalar * s1[i][j] + s2[i][j];
//	}
//
//	return dest;
//}
//
//

//
//
//struct fields& fields::operator=(struct fields&& rhs)
//{
//	size = rhs.size;
//	std::memmove(&comp, &rhs.comp, 3 * sizeof(void *));
//	rhs.comp = nullptr;
//
//	return *this;
//}
