#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include "NumArray.h"

static int newarray(lua_State *L);

static struct NumArray *checkarray(lua_State *L);

static double *getelem(lua_State *L);

static int setarray(lua_State *L);

static int getarray(lua_State *L);

static int getsize(lua_State *L);

static const struct luaL_Reg arraylib[] =
{
  {"new", newarray},
  {"set", setarray},
  {"get", getarray},
  {"size", getsize},
  {NULL, NULL}
};


struct NumArray *registerNumArray (
  lua_State *L,
  int nelements,
  const char *name)
{
	size_t nbytes = sizeof(struct NumArray) + (nelements - 1) *
	  sizeof(double);
	struct NumArray *array = lua_newuserdata(L, nbytes);
	array->size = nelements;

	/* Register the metatable so we can use the functionality associated
	   with this table */
	luaL_getmetatable(L, "array_mt");
	lua_setmetatable(L, -2);
	lua_setglobal(L, name);

	return array;
}


int luaUopen_array(lua_State *L)
{
	luaL_newmetatable(L, "array_mt");           // [0, 1, e]
	luaL_openlib(L, "array", arraylib, 0);

	lua_pushstring(L, "__index");
	lua_pushstring(L, "get");
	lua_gettable(L, 2); /* get array.get */     // [-1, +1, e]
	lua_settable(L, 1); /*metatable.__index = array.get*/  // [-2, 0, e]

	lua_pushstring(L, "__newindex");
	lua_pushstring(L, "set");
	lua_gettable(L, 2); /* get array.set */
	lua_settable(L, 1); /* metatable[__newindex] = array.set */

	return 1;
}


static int newarray(lua_State *L)
{
	int n = luaL_checkint(L, 1);
	size_t nbytes = sizeof(struct NumArray) + (n - 1) * sizeof(double);
	struct NumArray *a = (struct NumArray *) lua_newuserdata(L, nbytes);

	luaL_getmetatable(L, "array_mt");
	lua_setmetatable(L, -2);

	a->size = n;

	return 1;  /* new userdatum is already on the stack */
}


static struct NumArray *checkarray(lua_State *L)
{
	void *userdata = luaL_checkudata(L, 1, "array_mt");
	luaL_argcheck(L, userdata != NULL, 1, "Expected Array");

	return (struct NumArray*) userdata;
}


static double *getelem(lua_State *L)
{
	struct NumArray *a = checkarray(L);
	int index = luaL_checkint(L, 2);
	luaL_argcheck(L, 1 <= index && index <= a->size, 2,
	  "index out of range");

	return &a->values[index - 1];
}


static int setarray(lua_State *L)
{
	double value = luaL_checknumber(L, 3);
	*getelem(L) = value;

	return 0;
}


static int getarray(lua_State *L)
{
	lua_pushnumber(L, *getelem(L));
	return 1;
}


static int getsize(lua_State *L)
{
	struct NumArray *a = checkarray(L);
	lua_pushnumber(L, a->size);

	return 1;
}
