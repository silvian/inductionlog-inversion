#include "em_basics.hpp"
#include <boost/iterator/zip_iterator.hpp>

typedef vec3<double> d_vec3 ;

struct receiver
{
	d_vec3 polarization;
	double position;
};

struct measurement_geometry
{
	double source_position;
	d_vec3 source_polarization;
	std::vector<receiver> receivers;
	int nsize;
	int ref_counter;
};

static em_value green_derivative(d_vec3 x, zdouble k0)
{
	double dis = std::sqrt(x.norm());
	zdouble g = (i_ * k0 * dis) / (4. * M_PI * dis);
	zdouble dg = (-1. /pow(dis, 2) + i_ * k0 / dis) * g / dis;

	return dg * x;
}

class Position
{
public:
	dimarray const &dimensions;
	double const &h;
	double offset;

	Position(dimarray const &d, double const h) :
	  dimensions(d), h(h) 
	{
	}

	d_vec3 operator() (std::size_t i, std::size_t j, std::size_t k)
	{
		auto l = [=] (std::size_t ix, std::size_t n) {
			return h * (ix - .5 * dimensions[n] + 1.);
		};

		return d_vec3(l(i, 0) - offset, l(j, 1), l(k, 2));
	}	
};

class AdjointOperator : public emExpr<AdjointOperator>
{
public:
	std::vector<zdouble> const& _mu;
	std::shared_ptr<Window> _w;
	Position position;
	std::vector<receiver> const& receivers;

	AdjointOperator(std::vector<zdouble> const& mu, 
	  std::shared_ptr<Window>& w, struct measurement_geometry m) :
	  _mu(mu),
	  _w(w),
	  position(w->dimensions, w->stepsize),
	  receivers(m.receivers)
	{
	}

	vec2z _v_elem(std::size_t i, std::size_t j) __attribute_deprecated__
	{
		auto i_ = [&] (std::size_t j) {
			return j / (_w->dimensions[1] * _w->dimensions[2]);
		};
		auto j_ = [&] (std::size_t j) {
			return (j / _w->dimensions[2]) % _w->dimensions[1];
		};
		auto k_ = [&] (std::size_t j) {
			return j % _w->dimensions[0];
		};

		zdouble z1 = (*this)(i_(j), j_(j), k_(j))[i];
		j++;
		zdouble z2 = (*this)(i_(j), j_(j), k_(j))[i];
			
		vec2z out = {z1.real(), z1.imag(), z2.real(), z2.imag()};
		return out;
	}
	
	em_value operator() (size_t i, size_t j, size_t k)
	{	
		em_value acc(0., 0., 0.);

		size_t ix = 0;
		for (const auto &rec : receivers) {
			position.offset = rec.position;
			em_value dg = green_derivative(position(i, j, k),
			  _w->k0);
			acc -= cross(dg.conjugate(), _mu[ix++] * rec.polarization);
		}
			
		return acc * _w->getBackground();
	}

	operator em_fields()
	{
		em_fields result(_w);
	
		for (size_t i = 0; i < _w->dimensions[0]; i++)
		for (size_t j = 0; j < _w->dimensions[1]; j++)
		for (size_t k = 0; k < _w->dimensions[2]; k++)
			result(i, j, k) = (*this)(i, j, k);

		return result;
	}
};


template <typename E>
class MagneticOperator
{
	E const& _u;
 	struct measurement_geometry const& _m;
	Position position;

	/* Evaluate the curl of the vector potential A(x) at point (i, j, k) for 
	 * polarization (pol) */
	em_value operator() (std::size_t i, std::size_t j, std::size_t k, 
	  d_vec3 const& pol)
	{	
		em_value dg = green_derivative(position(i, j, k),
		  _u->getWindow()->k0);
		return inner_product(pol, cross(dg, _u(i, j, k)));
	}

	/* Numerically integrate the differentiated vector potential A(x) and
	 * scale to correct physical values*/
	zdouble synthesize_data(d_vec3 const& pol)
	{
		Window const *w = _u.getWindow().get();
		zdouble acc = 0.;
		for (std::size_t i = 0; i < w->dimensions[0]; i++)
		for (std::size_t j = 0; j < w->dimensions[1]; j++)
		for (std::size_t k = 0; k < w->dimensions[2]; k++)
			acc += (*this)(i, j, k, pol);
		
		return -acc * w->getBackground() * std::pow(w->stepsize, 3);
	}

public:	
	MagneticOperator(emExpr<E> const& u,
	  struct measurement_geometry const& m) : 
	  _u(u),
	  _m(m),
	  position(u.getWindow()->dimensions, u.getWindow()->stepsize)
	{
	}

	/* Evaluate the operator on _u for a given m upon implicit casting
	 * (fills the role of assingment) */
	operator std::vector<zdouble>()
	{	
		std::vector<zdouble> result(_m.receivers.size());
		
		for (size_t i = 0; i < result.size(); i++) {
			position.offset = _m.receivers[i].position;
			result[i] = synthesize_data(
			  _m.receivers[i].polarization);
		}

		return result;
	}
};
