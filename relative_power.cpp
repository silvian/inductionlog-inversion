#include "em_basics.hpp"


double relative_power (
  ElectricField const &E,
  MagneticField const &H,
  Contrast &X)
{
	ContrastAccess ca(X, E);
	double W = inner(X * E, E).real();
	zdouble S_bnd = boundary_integral(cross(conj(E), H));

	return std::abs(S_bnd) / W;
}
