

ScatterDistillReport iteration(ConductivityModel& m, Contrast& X, ContrastSource& cs, IncidentField& inc)
{
	w_field.iterate(X);
	model.minimize(inc + w_field.toScatteredField(), w_field);
	ScatterDistillReport = scatterDistillModels(model, distill_function);
}


double data_error(std::vector<zdouble> const& data, ContrastSource& cs);
double state_error(Contrast& X, em_field incident_field


class IncidentField : public emfields
{
	double cache_value;
	vec3 pol;
	double pos;

public:
	IncidentField(std::shared_ptr<Window> const& w, measurementGeometry const& m) :
		em_fields(w)
		cache_value(-1),
		pol(m.source_polarization),
		pos(s.source_position)
	{
	}

	operator(std::size_t i, std::size_t j)
	{
		if (window->getBackground() != cache_value)
			evaluate(window.get());
		return comp[i][j];
	}

}
