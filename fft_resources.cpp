#include "em_basics.hpp"

fft_resources::fft_resources(dimarray const &dim)
{
 	#ifdef _OPENMP
 	fftw_init_threads();
	#endif

	fftw_plan forward = fftw_plan_dft_3d(2 * dim[0], 2 * dim[1], 2 * dim[2], 
	  nullptr, 0x1, FFTW_FORWARD, FFTW_ESTIMATE);
	fftw_plan backward = fftw_plan_dft_3d(2 * dim[0], 2 * dim[1], 2 * dim[2],
	  nullptr, 0x1, FFTW_BACKWARD, FFTW_ESTIMATE);
	fftw_plan forward_inplace = fftw_plan_dft_3d(2 * dim[0], 2 * dim[1], 2 * dim[2], 
	  nullptr, nullptr, FFTW_FORWARD, FFTW_ESTIMATE);
	fftw_plan backward_inplace = fftw_plan_dft_3d(2 * dim[0], 2 * dim[1], 2 * dim[2],
	  nullptr, nullptr, FFTW_BACKWARD, FFTW_ESTIMATE);
}

void fft_resources::edit(dimarray const &dim)
{
	fftw_destroy_plan(forward);
	fftw_destroy_plan(backward);
	fftw_destroy_plan(forward_inplace);
	fftw_destroy_plan(backward_inplace);

	fftw_plan forward = fftw_plan_dft_3d(2 * dim[0], 2 * dim[1], 2 * dim[2], 
	  nullptr, 0x1, FFTW_FORWARD, FFTW_ESTIMATE);
	fftw_plan backward = fftw_plan_dft_3d(2 * dim[0], 2 * dim[1], 2 * dim[2],
	  nullptr, 0x1, FFTW_BACKWARD, FFTW_ESTIMATE);
	fftw_plan forward_inplace = fftw_plan_dft_3d(2 * dim[0], 2 * dim[1], 2 * dim[2], 
	  nullptr, nullptr, FFTW_FORWARD, FFTW_ESTIMATE);
	fftw_plan backward_inplace = fftw_plan_dft_3d(2 * dim[0], 2 * dim[1], 2 * dim[2],
	  nullptr, nullptr, FFTW_BACKWARD, FFTW_ESTIMATE);
}

fft_resources::~fft_resources()
{
	#ifdef _OPENMP
	fftw_cleanup_threads();
	#else
	fftw_cleanup();
	#endif
}
